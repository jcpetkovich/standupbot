# FROM openjdk:13-jre-alpine
FROM azul/zulu-openjdk-alpine:11-jre
COPY target/standupbot-1.0-SNAPSHOT.jar /usr/src/app/target/
COPY swagger.yaml /usr/src/app/
WORKDIR /usr/src/app/
ENTRYPOINT java -jar target/standupbot-1.0-SNAPSHOT.jar
