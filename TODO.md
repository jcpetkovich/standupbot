* insert standup for person
* insert a person
* get standups by person and time-range (limit, paging)
* get all standups by person (with a limit)
* get all standups for a team
* get all timestamps within a time-range
