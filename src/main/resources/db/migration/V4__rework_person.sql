ALTER TABLE person ALTER COLUMN name DROP NOT NULL;
ALTER TABLE person RENAME COLUMN name TO first_name;
ALTER TABLE person ADD COLUMN last_name varchar(100);
ALTER TABLE person ADD COLUMN username varchar(100) NOT NULL;
ALTER TABLE person ADD UNIQUE (username);
