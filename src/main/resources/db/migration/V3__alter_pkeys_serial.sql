create sequence person_id_seq owned by person.id;
alter table person alter column id set default nextval('person_id_seq');
create sequence standup_id_seq owned by standup.id;
alter table standup alter column id set default nextval('standup_id_seq');
