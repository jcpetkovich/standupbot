alter table person add primary key (id);

create table standup (
  id int not null primary key,
  person_id int REFERENCES person(id) ON DELETE CASCADE,
  FOREIGN KEY (person_id) REFERENCES person(id),
  yesterday varchar(1000),
  today varchar(1000),
  blocked varchar(1000)
);
