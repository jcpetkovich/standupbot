package io.ptk.standupbot.person;

import java.io.Serializable;
import java.util.Set;

import io.ptk.standupbot.daoapi.DBObj;
import io.ptk.standupbot.standup.Standup;

public class Person implements Serializable, DBObj {
  private static final long serialVersionUID = 1L;

  private Long id;
  private String username;
  private String firstName;
  private String lastName;
  private Set<Standup> standups;

  public Person() {
  }

  public Person(String username) {
    this.id = null;
    this.username = username;
  }

  public Person(long id, String username) {
    this.id = id;
    this.username = username;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Set<Standup> getStandups() {
    return standups;
  }

  public void setStandups(Set<Standup> standups) {
    this.standups = standups;
  }

  public String toString() {
    return "Person [id=" + this.id + ", " + "username=\"" + this.username + "\", " + "firstName=\"" + firstName + "\", "
        + "lastName=\"" + lastName + "\"" + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
    result = prime * result + ((standups == null) ? 0 : standups.hashCode());
    result = prime * result + ((username == null) ? 0 : username.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Person other = (Person) obj;
    if (firstName == null) {
      if (other.firstName != null)
        return false;
    } else if (!firstName.equals(other.firstName))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (lastName == null) {
      if (other.lastName != null)
        return false;
    } else if (!lastName.equals(other.lastName))
      return false;
    if (standups == null) {
      if (other.standups != null)
        return false;
    } else if (!standups.equals(other.standups))
      return false;
    if (username == null) {
      if (other.username != null)
        return false;
    } else if (!username.equals(other.username))
      return false;
    return true;
  }
}
