package io.ptk.standupbot.daoapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.PersistenceException;

import io.ptk.standupbot.person.Person;
import io.ptk.standupbot.standup.Standup;

public class StandupDao extends DaoImpl<Standup> {
  private static final String STANDUP_INSERT = "INSERT INTO standup (person_id, yesterday, today, blocked, datetime) VALUES (?, ?, ?, ?, ?)";
  private static final String STANDUP_ALL = "SELECT * FROM standup, person WHERE standup.person_id = person.id";
  private static final String STANDUP_ID_EXPRESSION = "id = (?)";
  private static final String STANDUP_PERSON_EXPRESSION = "person_id = (?)";
  private static final String STANDUP_DATE_EXPRESSION = "datetime BETWEEN SYMMETRIC (?) AND (?)";
  private static final String STANDUP_LIMIT_EXPRESSION = "LIMIT (?)";

  public StandupDao() {
  }

  public StandupDao(Connection dataSource) {
    this.dataSource = dataSource;
  }

  private void fillSetWithStandupsWithPerson(ResultSet rs, Set<Standup> standups, Person person) throws SQLException {
    while (rs.next()) {
      var s = this.extractFromResultSet(rs);
      s.setPerson(person);
      standups.add(s);
    }

    person.setStandups(standups);
  }

  protected Standup extractFromResultSet(ResultSet rs) throws SQLException {
    var s = new Standup();
    s.setYesterday(rs.getString("yesterday"));
    s.setToday(rs.getString("today"));
    s.setBlocked(rs.getString("blocked"));
    s.setDatetime(rs.getTimestamp("datetime").toInstant());
    s.setId(rs.getLong("id"));
    return s;
  }

  protected void fillInsertStatement(PreparedStatement statement, Standup standup) throws SQLException {
    statement.setLong(1, standup.getPerson().getId());
    statement.setString(2, standup.getYesterday());
    statement.setString(3, standup.getToday());
    statement.setString(4, standup.getBlocked());
    statement.setTimestamp(5, Timestamp.from(standup.getDatetime()));
  }

  public Optional<Standup> get(long id) {
    var query = STANDUP_ALL + " WHERE " + STANDUP_ID_EXPRESSION;
    return this.getById(id, query);
  }

  public Set<Standup> getAll() {
    var query = STANDUP_ALL;
    Set<Standup> standups = this.getAllByTable(query);

    // collapse people
    var people = new HashMap<Long, Person>();
    for (var s : standups) {
      Long personId = s.getPerson().getId();
      if (people.containsKey(personId)) {
        s.setPerson(people.get(personId));
      } else {
        people.put(personId, s.getPerson());
      }
    }
    return standups;
  }

  public Set<Standup> getByPersonBetweenDate(Person person, Instant startDate, Instant endDate) {
    var query = STANDUP_ALL + " AND " + STANDUP_PERSON_EXPRESSION + " AND " + STANDUP_DATE_EXPRESSION;
    var standups = new HashSet<Standup>();

    try (var statement = this.dataSource.prepareStatement(query)) {
      statement.setLong(1, person.getId());
      statement.setTimestamp(2, Timestamp.from(startDate));
      statement.setTimestamp(3, Timestamp.from(endDate));

      var rs = statement.executeQuery();

      this.fillSetWithStandupsWithPerson(rs, standups, person);

    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return standups;
  }

  public Set<Standup> getByPersonWithLimit(Person person, int limit) {
    var query = STANDUP_ALL + " AND " + STANDUP_PERSON_EXPRESSION + " " + STANDUP_LIMIT_EXPRESSION;
    var standups = new HashSet<Standup>();

    try (var statement = this.dataSource.prepareStatement(query)) {
      statement.setLong(1, person.getId());
      statement.setLong(2, limit);
      var rs = statement.executeQuery();

      this.fillSetWithStandupsWithPerson(rs, standups, person);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return standups;
  }

  public void save(Standup standup) throws PersistenceException {
    this.insert(standup, STANDUP_INSERT);
  }

  public void saveAll(Set<Standup> standups) throws PersistenceException {
    for (var s : standups) {
      if (s.getPerson() == null) {
        throw new NullPointerException("standup.getPerson() cannot be null");
      }

      if (s.getDatetime() == null) {
        throw new NullPointerException("standup.getDatetime() cannot be null");
      }
    }
    this.insertBatch(standups, STANDUP_INSERT);
  }

  // fill standups by person and time-range(limit,paging)
  public Person fillStandupsBetweenDate(Person person, Instant start, Instant end) {
    this.getStandupsByPersonBetweenDate(person, start, end);
    return person;
  }

  // get standups by person and time-range(limit,paging)
  public Set<Standup> getStandupsByPersonBetweenDate(Person person, Instant start, Instant end) {
    return this.getByPersonBetweenDate(person, start, end);
  }

  // fill all standups by person(with a limit)
  public Person fillPersonRecentStandupsWithLimit(Person person, int limit) {
    this.getRecentStandupsByPersonWithLimit(person, limit);
    return person;
  }

  // get all standups by person(with a limit)
  public Set<Standup> getRecentStandupsByPersonWithLimit(Person person, int limit) {
    return this.getByPersonWithLimit(person, limit);
  }

  // TODO: get all standups for a team
  // TODO: get all timestamps within a time-range
}
