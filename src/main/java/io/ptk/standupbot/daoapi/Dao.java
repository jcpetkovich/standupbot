package io.ptk.standupbot.daoapi;

import java.util.Optional;
import java.util.Set;

import javax.persistence.PersistenceException;

public interface Dao<T> {
  Optional<T> get(long id);

  Set<T> getAll();

  void save(T t) throws PersistenceException;

  void saveAll(Set<T> t) throws PersistenceException;

  // void update(T t, String[] params);

  // void delete(T t);
}
