package io.ptk.standupbot.daoapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Set;

import javax.persistence.PersistenceException;

import io.ptk.standupbot.person.Person;

public class PersonDao extends DaoImpl<Person> {
  private static final String PERSON_INSERT = "INSERT INTO person (first_name, last_name, username) VALUES (?, ?, ?)";
  private static final String PERSON_ALL = "SELECT * FROM person";
  private static final String PERSON_ID_EXPRESSION = "id = (?)";
  private static final String PERSON_USERNAME_EXPRESSION = "username = (?)";

  public PersonDao() {
  }

  public PersonDao(Connection dataSource) {
    this.dataSource = dataSource;
  }

  protected Person extractFromResultSet(ResultSet rs) throws SQLException {
    var p = new Person();
    p.setFirstName(rs.getString("first_name"));
    p.setLastName(rs.getString("last_name"));
    p.setUsername(rs.getString("username"));
    p.setId(rs.getLong("id"));
    return p;
  }

  protected void fillInsertStatement(PreparedStatement statement, Person person) throws SQLException {
    statement.setString(1, person.getFirstName());
    statement.setString(2, person.getLastName());
    statement.setString(3, person.getUsername());
  }

  public Optional<Person> get(long id) {
    var query = PERSON_ALL + " WHERE " + PERSON_ID_EXPRESSION;
    return this.getById(id, query);
  }

  public Optional<Person> getByUsername(String username) {
    var query = PERSON_ALL + " WHERE " + PERSON_USERNAME_EXPRESSION;

    Optional<Person> person = Optional.empty();
    try (var statement = this.dataSource.prepareStatement(query)) {
      statement.setString(1, username);
      var rs = statement.executeQuery();
      rs.next();
      person = Optional.of(extractFromResultSet(rs));

    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return person;
  }

  public Set<Person> getAll() {
    var query = PERSON_ALL;
    return this.getAllByTable(query);
  }

  public void save(Person person) throws PersistenceException {
    this.insert(person, PERSON_INSERT);
  }

  public void saveAll(Set<Person> persons) throws PersistenceException {
    this.insertBatch(persons, PERSON_INSERT);
  }

}
