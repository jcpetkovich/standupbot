package io.ptk.standupbot.daoapi;

public interface DBObj {
  Long getId();
  void setId(long id);
}
