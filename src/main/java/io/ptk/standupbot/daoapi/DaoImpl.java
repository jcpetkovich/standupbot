package io.ptk.standupbot.daoapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import javax.persistence.PersistenceException;

public abstract class DaoImpl<T extends DBObj> implements Dao<T> {
  protected Connection dataSource;

  protected abstract T extractFromResultSet(ResultSet rs) throws SQLException;

  protected abstract void fillInsertStatement(PreparedStatement statement, T t) throws SQLException;

  public void setDataSource(Connection dataSource) {
    this.dataSource = dataSource;
  }

  public Connection getDataSource() {
    return this.dataSource;
  }

  protected void insert(T t, String insertSql) throws PersistenceException {
    var query = insertSql;
    try (var statement = this.dataSource.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

      fillInsertStatement(statement, t);

      int affectedRows = statement.executeUpdate();
      if (affectedRows == 0)
        throw new SQLException("Creating person failed, no rows affected.");

      try (var generatedKeys = statement.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          t.setId(generatedKeys.getLong(1));
        } else {
          throw new SQLException("Creating person failed, no ID returned.");
        }
      }

    } catch (SQLException e) {
      throw new PersistenceException("Error during insert of item " + t);
    }
  }

  protected void insertBatch(Set<T> ts, String insertSql) throws PersistenceException {
    var query = insertSql;

    var tsordered = new ArrayList<T>();

    tsordered.addAll(ts);

    try (var statement = this.dataSource.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
      for (var t : tsordered) {
        fillInsertStatement(statement, t);
        statement.addBatch();
      }
      int[] affectedRows = statement.executeBatch();
      if (IntStream.of(affectedRows).sum() == 0)
        throw new SQLException("Creating persons failed, no rows affected.");

      try (var generatedKeys = statement.getGeneratedKeys()) {
        int i = 0;
        while (generatedKeys.next()) {
          tsordered.get(i).setId(generatedKeys.getLong(1));
          i++;
        }
      }
    } catch (SQLException e) {
      throw new PersistenceException("Error inserting bulkset " + ts);
    }
  }

  protected Optional<T> getById(long id, String getSql) {
    Optional<T> t = Optional.empty();
    try (var statement = this.dataSource.prepareStatement(getSql)) {
      statement.setLong(1, id);
      var rs = statement.executeQuery();
      rs.next();
      t = Optional.of(extractFromResultSet(rs));

    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return t;
  }

  protected Set<T> getAllByTable(String getAllSql) {
    var ts = new HashSet<T>();
    try (var statement = this.dataSource.prepareStatement(getAllSql)) {
      var rs = statement.executeQuery();
      while (rs.next()) {
        ts.add(extractFromResultSet(rs));
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return ts;
  }
}
