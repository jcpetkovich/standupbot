package io.ptk.standupbot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Set;

import com.fasterxml.jackson.core.type.TypeReference;

import org.flywaydb.core.Flyway;

import io.ptk.standupbot.daoapi.PersonDao;
import io.ptk.standupbot.person.Person;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.core.json.jackson.JacksonCodec;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;

public class App extends AbstractVerticle {

  private static final String DB_DRIVER = "org.postgresql.Driver";
  private static final String DB_HOST = System.getenv("POSTGRES_HOST");
  private static final String DB_PORT = System.getenv("POSTGRES_PORT");
  private static final String DB_USER = System.getenv("POSTGRES_USER");
  private static final String DB_PASSWORD = System.getenv("POSTGRES_PASSWORD");
  private static final String DB_NAME = System.getenv("POSTGRES_DB");
  private static final String DB_CONNECTION = "jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME;

  private Connection dataSource;

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    // init/migrate database if it hasn't been
    var flyway = Flyway.configure().dataSource(DB_CONNECTION, DB_USER, DB_PASSWORD).load();

    flyway.migrate();

    dataSource = getDBConnection();
    // var pdao = new PersonDao(dataSource);
    // var person = new Person("Bob");

    // pdao.save(person);

    OpenAPI3RouterFactory.create(vertx, "swagger.yaml", ar -> {
      if (ar.succeeded()) {
        // Spec loaded with success
        var routerFactory = ar.result();

        routerFactory.addHandlerByOperationId("listPersons", this::listPersonsController);

        routerFactory.addHandlerByOperationId("createPersons", this::createPersonsController);

        Router router = routerFactory.getRouter();

        vertx.createHttpServer().requestHandler(router).listen(8080);

      } else {
        // Something went wrong during router factory initialization
        System.out.println("Holy shit that didn't work at all.");
        Throwable exception = ar.cause();
        exception.printStackTrace();
      }

    });

  }

  private void listPersonsController(RoutingContext routingContext) {

    var pdao = new PersonDao(this.dataSource);
    var persons = pdao.getAll();

    routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
        .end(Json.encodePrettily(persons));

  }

  private void createPersonsController(RoutingContext routingContext) {

    final Set<Person> persons = JacksonCodec.decodeValue(routingContext.getBodyAsString(),
        new TypeReference<Set<Person>>() {
        });

    var pdao = new PersonDao(dataSource);

    try {
      pdao.saveAll(persons);
    } catch (Exception e) {
      routingContext.response().setStatusCode(400).putHeader("content-type", "application/json; charset=utf-8")
          .end(routingContext.getBodyAsString());
      return;
    }
    System.out.println(persons);

    // Return the created whisky as JSON
    routingContext.response().setStatusCode(201).putHeader("content-type", "application/json; charset=utf-8")
        .end(Json.encodePrettily(persons));

  }

  private static Connection getDBConnection() {
    Connection dataSource = null;
    try {
      Class.forName(DB_DRIVER);
    } catch (ClassNotFoundException e) {
      System.out.println(e);
    }

    try {
      dataSource = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return dataSource;
  }
}
