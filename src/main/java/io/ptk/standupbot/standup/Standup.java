package io.ptk.standupbot.standup;

import java.io.Serializable;
import java.time.Instant;

import io.ptk.standupbot.daoapi.DBObj;
import io.ptk.standupbot.person.Person;

public class Standup implements Serializable, DBObj {
  private static final long serialVersionUID = 1L;

  private Long id;
  private String yesterday;
  private String today;
  private String blocked;
  private Instant datetime;
  private Person person;

  public Standup() {
  }

  public Standup(Long id, String yesterday, String today, String blocked, Instant datetime) {
    this.id = id;
    this.yesterday = yesterday;
    this.today = today;
    this.blocked = blocked;
    this.datetime = datetime;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getYesterday() {
    return this.yesterday;
  }

  public void setYesterday(String yesterday) {
    this.yesterday = yesterday;
  }

  public String getToday() {
    return this.today;
  }

  public void setToday(String today) {
    this.today = today;
  }

  public String getBlocked() {
    return this.blocked;
  }

  public void setBlocked(String blocked) {
    this.blocked = blocked;
  }

  public Instant getDatetime() {
    return this.datetime;
  }

  public void setDatetime(Instant datetime) {
    this.datetime = datetime;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public String toString() {
    String standup = "Standup [id=" + this.id + ", " + "yesterday=\"" + this.yesterday + "\", " + "today=\""
        + this.today + "\", " + "blocked=\"" + this.blocked + "\", " + "datetime=\"" + this.datetime + "\"]";

    return standup;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((blocked == null) ? 0 : blocked.hashCode());
    result = prime * result + ((datetime == null) ? 0 : datetime.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((person == null) ? 0 : person.hashCode());
    result = prime * result + ((today == null) ? 0 : today.hashCode());
    result = prime * result + ((yesterday == null) ? 0 : yesterday.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Standup other = (Standup) obj;
    if (blocked == null) {
      if (other.blocked != null)
        return false;
    } else if (!blocked.equals(other.blocked))
      return false;
    if (datetime == null) {
      if (other.datetime != null)
        return false;
    } else if (!datetime.equals(other.datetime))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (person == null) {
      if (other.person != null)
        return false;
    } else if (!person.equals(other.person))
      return false;
    if (today == null) {
      if (other.today != null)
        return false;
    } else if (!today.equals(other.today))
      return false;
    if (yesterday == null) {
      if (other.yesterday != null)
        return false;
    } else if (!yesterday.equals(other.yesterday))
      return false;
    return true;
  }
}
