package io.ptk.standupbot.serialization;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class Serialization {
  public static ObjectMapper getJsonMapper() {
    ObjectMapper mapper = new ObjectMapper();

    SimpleModule emptyStringToNullModule = new SimpleModule("EmptyStringToNullModule", Version.unknownVersion())
        .addDeserializer(String.class, new EmptyStringDeserializer());
    mapper.registerModule(emptyStringToNullModule);

    return mapper;
  }
}
