package io.ptk.standupbot.person;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;

import io.ptk.standupbot.serialization.Serialization;

public class PersonTest {
  @Test
  public void testEmptyConstructor() {
    var p = new Person();
    assertTrue(p.getUsername() == null);
    assertTrue(p.getId() == null);
  }

  @Test
  public void testNameConstructor() {
    var p = new Person("bdawg");
    assertEquals(p.getUsername(), "bdawg");
    assertTrue(p.getId() == null);
  }

  @Test
  public void testFullConstructor() {
    var p = new Person(0, "bman");
    assertEquals(p.getUsername(), "bman");
    assertEquals(p.getId(), (Long) 0L);
  }

  @Test
  public void testJsonMarshaling() {
    var p = new Person();
    p.setUsername("bobby");

    try {
      var mapper = Serialization.getJsonMapper();
      var m = mapper.writeValueAsString(p);
      assertTrue(m.contains("\"username\":\"bobby\""));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Test
  public void testJsonUnmarshalling() {
    var s = "{\"id\":null,\"username\":null,\"firstName\":null,\"lastName\":null,\"standups\":null}";

    try {
      var mapper = Serialization.getJsonMapper();
      Person m = mapper.readValue(s, Person.class);

      assertNull(m.getUsername());
      assertNull(m.getFirstName());
      assertNull(m.getLastName());
      assertNull(m.getStandups());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
