package io.ptk.standupbot.person;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import io.ptk.standupbot.person.Person;
import io.ptk.standupbot.daoapi.PersonDao;
import io.ptk.standupbot.serialization.Serialization;
import io.ptk.standupbot.utils.TestDBMixin;

public class PersonDaoTest extends TestDBMixin {

  @Test
  public void basic() {
    assertEquals(true, true);
  }

  @Test
  public void testSavePerson() throws SQLException {
    var dbconn = getDBConnection();
    var pdao = new PersonDao(dbconn);
    var targetun = "brichter";
    var person = new Person(targetun);

    pdao.save(person);

    var rs = execQuery(dbconn, "select * from person where username = '" + targetun + "'");

    while (rs.next()) {
      var personName = rs.getString("username");
      assertEquals(personName, targetun);
    }

    rs = execQuery(dbconn, "select * from person where id = " + person.getId() + "");

    while (rs.next()) {
      var personName = rs.getString("username");
      assertEquals(personName, targetun);
    }
  }

  @Test
  public void testSaveAll() throws SQLException {
    var dbconn = getDBConnection();
    var pdao = new PersonDao(dbconn);
    String[] usernames = { "ddawg", "bman", "jfin" };
    var persons = new HashSet<Person>();

    for (String un : usernames) {
      persons.add(new Person(un));
    }

    var rs = execQuery(dbconn, "select count(*) from person");
    rs.next();
    int countBefore = rs.getInt("count");
    rs.close();

    pdao.saveAll(persons);

    rs = execQuery(dbconn, "select count(*) from person");
    rs.next();
    int countAfter = rs.getInt("count");
    rs.close();

    assertEquals(countBefore + 3, countAfter);
  }

  @Test
  public void testLoadAllPerson() throws SQLException {
    var dbconn = getDBConnection();
    var pdao = new PersonDao(dbconn);
    String[] usernames = { "ddawgwi", "bman48", "jfin32" };
    var persons = new HashSet<Person>();

    for (String un : usernames) {
      persons.add(new Person(un));
    }

    pdao.saveAll(persons);

    var rs = execQuery(dbconn, "select count(*) from person");
    rs.next();
    int count = rs.getInt("count");
    rs.close();

    var otherPersons = pdao.getAll();

    int i = 0;
    for (var p : otherPersons) {
      i++;
    }

    assertEquals(i, count);
  }

  @Test
  public void testGetPerson() throws SQLException {
    var dbconn = getDBConnection();
    var pdao = new PersonDao(dbconn);
    String[] usernames = { "dillydally", "bobbybally", "jimbo" };
    var persons = new HashSet<Person>();

    for (String un : usernames) {
      persons.add(new Person(un));
    }

    pdao.saveAll(persons);

    var person = pdao.get(2);

    assertEquals(person.orElse(new Person()).getFirstName(), "Bill", "Check that we found Bill");

    for (var p : persons) {
      var retrievedPerson = pdao.get(p.getId());
      assertEquals(retrievedPerson.orElse(new Person()).getUsername(), p.getUsername(),
          "Check that ids are in the right order");
    }
  }

  @Test
  public void testSavePersonFromJson() throws SQLException {
    var dbconn = getDBConnection();
    var pdao = new PersonDao(dbconn);
    var targetun = "frichter";
    var jsonString = "{\"id\":null,\"username\":\"" + targetun
        + "\",\"firstName\":null,\"lastName\":null,\"standups\":null}";

    try {
      var mapper = Serialization.getJsonMapper();
      Person person = mapper.readValue(jsonString, Person.class);
      pdao.save(person);
      var rs = execQuery(dbconn, "select * from person where username = '" + targetun + "'");

      while (rs.next()) {
        var personName = rs.getString("username");
        assertEquals(personName, targetun);
      }

      rs = execQuery(dbconn, "select * from person where id = " + person.getId() + "");

      while (rs.next()) {
        var personName = rs.getString("username");
        assertEquals(personName, targetun);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
