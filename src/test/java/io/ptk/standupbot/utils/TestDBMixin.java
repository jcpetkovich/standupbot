package io.ptk.standupbot.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.ext.ScriptUtils;
import org.testcontainers.jdbc.ContainerLessJdbcDelegate;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public class TestDBMixin {
  private static final String DB_DRIVER = "org.postgresql.Driver";

  @Container
  public static final PostgreSQLContainer postgres = new PostgreSQLContainer<>("postgres:11.2-alpine");

  @BeforeAll
  public static void setUp() throws ClassNotFoundException, SQLException {
    Class.forName(DB_DRIVER);
    var dbString = postgres.getJdbcUrl();

    var flyway =
        Flyway.configure()
            .dataSource(dbString, postgres.getUsername(), postgres.getPassword())
            .load();

    var migrations = flyway.migrate();

    try (var dbconn = getDBConnection()) {
      loadTestData(dbconn);
    }
  }

  public static Connection getDBConnection() {
    Connection dataSource = null;

    try {
      dataSource =
          DriverManager.getConnection(
              postgres.getJdbcUrl(), postgres.getUsername(), postgres.getPassword());
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    return dataSource;
  }

  public static ResultSet execQuery(Connection dataSource, String sql) throws SQLException {
    var statement = dataSource.createStatement();
    var rs = statement.executeQuery(sql);
    return rs;
  }

  public static void loadTestData(Connection dataSource) throws SQLException {
    ScriptUtils.runInitScript(
        new ContainerLessJdbcDelegate(dataSource), "io/ptk/standupbot/test_input.sql");
  }
}
