package io.ptk.standupbot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(VertxExtension.class)
class SampleVerticleTest {

  @Test
  @DisplayName("⏱ Count 3 timer ticks")
  void countThreeTicks(Vertx vertx, VertxTestContext testContext) {
    AtomicInteger counter = new AtomicInteger();
    vertx.setPeriodic(100, id -> {
      if (counter.incrementAndGet() == 3) {
        testContext.completeNow();
      }
    });
  }

  @Test
  @DisplayName("⏱ Count 3 timer ticks, with a checkpoint")
  void countThreeTicksWithCheckpoints(Vertx vertx, VertxTestContext testContext) {
    Checkpoint checkpoint = testContext.checkpoint(3);
    vertx.setPeriodic(100, id -> checkpoint.flag());
  }

  // @Test
  // @DisplayName("🚀 Deploy a HTTP service verticle and make 10 requests")
  // void useSampleVerticle(Vertx vertx, VertxTestContext testContext) {
  //   WebClient webClient = WebClient.create(vertx);
  //   Checkpoint deploymentCheckpoint = testContext.checkpoint();
  //   Checkpoint requestCheckpoint = testContext.checkpoint(10);

  //   vertx.deployVerticle(new App(), testContext.succeeding(id -> {
  //     deploymentCheckpoint.flag();

  //     for (int i = 0; i < 10; i++) {
  //       webClient.get(8080, "localhost", "/").as(BodyCodec.string()).send(testContext.succeeding(resp -> {
  //         testContext.verify(() -> {
  //           assertThat(resp.statusCode()).isEqualTo(200);
  //           assertThat(resp.body()).contains("Hello");
  //           requestCheckpoint.flag();
  //         });
  //       }));
  //     }
  //   }));
  // }
}
