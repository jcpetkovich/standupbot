package io.ptk.standupbot.standup;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import io.ptk.standupbot.daoapi.PersonDao;
import io.ptk.standupbot.daoapi.StandupDao;
import io.ptk.standupbot.person.Person;
import io.ptk.standupbot.utils.TestDBMixin;

public class StandupDaoTest extends TestDBMixin {

  @Test
  public void basic() {
    assertEquals(true, true);
  }

  @Test
  public void testSaveStandup() throws SQLException {
    var dbconn = getDBConnection();
    var sdao = new StandupDao(dbconn);
    var pdao = new PersonDao(dbconn);
    var standup = new Standup();
    var person = new Person();
    var target = "didn't do much";

    person.setFirstName("Jean-Christophe");
    person.setLastName("Petkovich");
    person.setUsername("jcpetkovich");

    standup.setYesterday(target);
    standup.setToday("not planning on doing much today either");
    standup.setBlocked("woah dude, this is bad, I'm blocked on everything");
    standup.setDatetime(Instant.now());
    standup.setPerson(person);

    pdao.save(person);
    sdao.save(standup);

    var rs = execQuery(dbconn, "select * from standup where id = " + standup.getId() + "");

    while (rs.next()) {
      var standupYesterday = rs.getString("yesterday");
      assertEquals(target, standupYesterday);
    }

  }

  @Test
  public void testSaveAll() throws SQLException {
    var dbconn = getDBConnection();
    var sdao = new StandupDao(dbconn);
    var pdao = new PersonDao(dbconn);

    var person = new Person();
    person.setFirstName("Billy");
    person.setLastName("Mays");
    person.setUsername("wickedsales");

    String[] todays = { "sieze the day", "I'll work on it tomorrow", "Don't worry about it" };
    String[] yesterdays = { "didn't do much", "did everything", "did half" };
    String[] blockeds = { "blocked on myself", "blocked on you", "blocked on tomorrow????" };
    var standups = new HashSet<Standup>();
    for (int i = 0; i < todays.length; i++) {
      var s = new Standup();
      s.setToday(todays[i]);
      s.setYesterday(yesterdays[i]);
      s.setBlocked(blockeds[i]);
      s.setPerson(person);
      s.setDatetime(Instant.now());
      standups.add(s);
    }

    pdao.save(person);
    sdao.saveAll(standups);

    var zoneId = ZoneId.systemDefault();
    var zdt = ZonedDateTime.ofInstant(Instant.now(), zoneId);
    var zdtStart = zdt.toLocalDate().atStartOfDay();
    var zdtTomorrowStart = zdtStart.plusDays(1);
    var zoneOffset = zoneId.getRules().getOffset(Instant.now());
    var instantStart = zdtStart.toInstant(zoneOffset);
    var instantEnd = zdtTomorrowStart.toInstant(zoneOffset);

    var returnedStandups = sdao.getByPersonBetweenDate(person, instantStart, instantEnd);

    for (var expectedStandup : standups) {
      var found = false;
      for (var returnedStandup : returnedStandups) {
        if (expectedStandup.equals(returnedStandup)) {
          found = true;
        }
      }
      assertTrue(found);
    }
  }
}
