package io.ptk.standupbot.standup;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class StandupTest {
  @Test
  public void testEmptyConstructor() {
    var s = new Standup();
    assertTrue(s.getPerson() == null);
    assertTrue(s.getId() == null);
  }
}
