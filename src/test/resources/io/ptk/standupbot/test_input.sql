INSERT INTO person (first_name, last_name, username) VALUES
('Bob', 'Barker', 'bbarker'),
('Bill', 'Bernstein', 'bberny'),
('Jill', 'Johnstone', 'japes'),
('Jane', 'Dill', 'jdill');

WITH insertion (yesterday, today, blocked, username) AS
    (VALUES
    ('* finished working on test data\n* wrote new implementation',
    '* working on a new structure for pulls',
    '* blocked on Jill',
    'bbarker'
    ),
    ('* A\n* B\n* C',
    '* one\n* two\n* three',
    '* blocked on Bob',
    'bbernv'
    ),
    ('* A\n* B\n* C',
    '* one\n* two\n* three',
    '* blocked on Tom',
    'japes'
    ),
    ('* A\n* B\n* C',
    '* one\n* two\n* three',
    '* blocked on Bill',
    'jdill'
    )
      )
INSERT INTO standup
    (person_id, yesterday, today, blocked)
SELECT
  person.id, insertion.yesterday, insertion.today, insertion.blocked
  FROM
      person JOIN insertion
      ON person.username = insertion.username ;
